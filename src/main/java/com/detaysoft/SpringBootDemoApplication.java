package com.detaysoft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.detaysoft.service.UserService;

@SpringBootApplication
@EnableScheduling
@ComponentScan({"com.detaysoft","com.detaysoft.tasks","com.detaysoft.security"})
public class SpringBootDemoApplication {

	@Autowired
	UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemoApplication.class, args);
	}
	
	@Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {            
            userService.addUser("erdem", "123456");
            userService.addUser("maho", "123456");
        };
    }
}
