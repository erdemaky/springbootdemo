package com.detaysoft.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.detaysoft.service.JobRequestService;

@Component
public class ScheduledTasks {

	@Autowired
	private JobRequestService jobRequestService;
	
	@Scheduled(fixedRate = 1000000)
	public void deleteInactiveJobs() {
		jobRequestService.deleteInactiveJobs();
	}
	
}
