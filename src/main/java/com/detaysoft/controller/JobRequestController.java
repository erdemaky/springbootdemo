package com.detaysoft.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.detaysoft.entity.JobRequest;
import com.detaysoft.service.JobRequestService;

@RestController
public class JobRequestController {

	private static final Logger LOG = LoggerFactory.getLogger(JobRequestController.class);
			
	@Autowired
	private JobRequestService jobRequestService;

	@RequestMapping(value = "/showjobs", method = RequestMethod.GET)
	@ResponseBody
	public Iterable<JobRequest> getAllJobAndAplly() {
		LOG.info("getAllJobAndAplly triggered");
		return jobRequestService.getAllJobs();
	}

	@RequestMapping(value = "/addjob", method = RequestMethod.PUT)
	public JobRequest addJob(JobRequest request) {
		LOG.info("addJob triggered");
		JobRequest addJob = jobRequestService.addJob(request);

		return addJob;
	}

}
