package com.detaysoft.controller;

import javax.ws.rs.QueryParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.detaysoft.service.UserService;

@RestController
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
			
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/getuser", method = RequestMethod.GET)
	public String getUsers(@QueryParam("mail") String mail) {
		LOG.info("getUsers triggered");
		return userService.getUser(mail);
	}
	
	@RequestMapping(value = "/adduser", method = RequestMethod.GET)
	public String addUser(@QueryParam("mail") String mail,@QueryParam("password") String password) {
		LOG.info("addUser triggered");
		return userService.addUser(mail, password);
	}
	
}
