package com.detaysoft.service;

public interface AuthService {

	boolean authUser(String username, String credential);
}
