package com.detaysoft.service;

import com.detaysoft.entity.JobRequest;

public interface JobRequestService {

	public Iterable<JobRequest> getAllJobs();
	
	public JobRequest addJob(JobRequest jobRequest);
	
	public void deleteInactiveJobs();
}
