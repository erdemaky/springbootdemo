package com.detaysoft.service;

import com.detaysoft.entity.User;

public interface UserService {

	public String addUser(String mail, String password);
	
	public String getUser(String mail);
	
	public Iterable<User> getAllUsers();
	
}
