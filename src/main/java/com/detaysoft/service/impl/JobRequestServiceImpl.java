package com.detaysoft.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.detaysoft.entity.JobRequest;
import com.detaysoft.repository.JobRequestRepository;
import com.detaysoft.service.JobRequestService;

@Service
public class JobRequestServiceImpl implements JobRequestService {

	private static final Logger LOG = LoggerFactory.getLogger(JobRequestServiceImpl.class);
			
	//FIXME private tag
	@Autowired
	private JobRequestRepository jobRequestRepository;
	
	@Override
	public Iterable<JobRequest> getAllJobs() {
		return jobRequestRepository.findAll();
	}

	@Override
	public JobRequest addJob(JobRequest jobRequest) {
		JobRequest dbObject = jobRequestRepository.save(jobRequest);
		
		if (dbObject != null){
			return dbObject;
		}else{
			LOG.warn("Parametre NULL addJob");
			return null;
		}
	}

	@Override
	public void deleteInactiveJobs() {
		Iterable<JobRequest> requests =  jobRequestRepository.findAll();
		
		for (JobRequest jobRequest : requests) {
			if (jobRequest.getActive() == false){
				jobRequestRepository.delete(jobRequest);
			}
		}
	}

	
	
}
