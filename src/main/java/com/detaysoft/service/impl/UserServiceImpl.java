package com.detaysoft.service.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.detaysoft.entity.User;
import com.detaysoft.repository.UserRepository;
import com.detaysoft.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
			
	// FIXME gerekli olmamasına rağmen okunabilirlik açısından private tag'i
	// koyalım :*
	@Autowired
	private UserRepository userRepository;

	@Override
	public String addUser(String mail, String password) {
		User user = new User();
		user.setMail(mail);

		//kullanıcı parolasını da md5'li kaydediyoruz
		
		MessageDigest md5Digest;
		try {
			md5Digest = MessageDigest.getInstance("MD5");
			md5Digest.update(password.getBytes(), 0, password.length());
			user.setParola(new BigInteger(1, md5Digest.digest()).toString(16));
		} catch (NoSuchAlgorithmException e) {
			user.setParola(password);
			LOG.error("addUser hata", e);
		}

		user = userRepository.save(user);

		return user.getMail() + " eklendi.";
	}

	@Override
	public String getUser(String mail) {

		Set<User> users = userRepository.findByMailContaining(mail);

		for (User user : users) {
			return user.getParola();
		}

		return "Boş";
	}

	@Override
	public Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}

}
