package com.detaysoft.service.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.detaysoft.entity.User;
import com.detaysoft.repository.UserRepository;
import com.detaysoft.service.AuthService;

@Service
public class AuthServiceImpl implements AuthService {

	private static final Logger LOG = LoggerFactory.getLogger(AuthServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public boolean authUser(String email, String credential) {
		User dbUser = userRepository.findByMailEquals(email);

		MessageDigest md5Digest;
		try {
			md5Digest = MessageDigest.getInstance("MD5");
			md5Digest.update(credential.getBytes(), 0, credential.length());
			if (dbUser.getParola().equals(new BigInteger(1, md5Digest.digest()).toString(16))) {
				return true;
			}
		} catch (NoSuchAlgorithmException e) {
			if (dbUser.getParola().equals(credential)) {
				return true;
			}
			LOG.error("Kullanıcı doğrulama hata ", e);
		}
		return false;
	}

}
