package com.detaysoft.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "user")
public class User {

	private int id;
	private String mail; // unique
	//değişken ve metod isimleri, başka bir sistemden import etmediğimiz sürece ingilizce olmalı.
	private String password; // md5
	//abi bunu unutmustum değiştirmeye üşendim :(((

	@Id
	@Column(name = "user_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(unique = true)
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getParola() {
		return password;
	}

	public void setParola(String parola) {
		this.password = parola;
	}

}
