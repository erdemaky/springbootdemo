package com.detaysoft.repository;


import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.entity.User;


public interface UserRepository extends CrudRepository<User, Long>{

	//FIXME bir alana göre aramada Equals yada containing kullanmalısın
	// query method'lar için şuna bir göz atabilirsin. https://www.petrikainulainen.net/programming/spring-framework/spring-data-jpa-tutorial-introduction-to-query-methods/
	
	User findByMailEquals(String mail);
	
	Set<User> findByMailContaining(String mail);
	
	
}
