package com.detaysoft.repository;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.entity.Apply;

public interface ApplyRepository extends CrudRepository<Apply, Long>{

}
