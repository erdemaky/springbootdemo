package com.detaysoft.repository;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.entity.JobRequest;

public interface JobRequestRepository extends CrudRepository<JobRequest, Long>{

}
