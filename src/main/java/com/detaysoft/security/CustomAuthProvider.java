package com.detaysoft.security;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.detaysoft.service.AuthService;


@Component
public class CustomAuthProvider implements AuthenticationProvider {

	@Autowired
	private AuthService authService;
	
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        if(name==null || password==null){
        	return null;
        }else{
        	if(authService.authUser(name, password)){
        		 return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
        	}else{
        		return null;
        	}
        }
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}